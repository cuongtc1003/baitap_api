
// bật loading
function batLoading() {
    document.getElementById('loading').style.display='flex';
    document.getElementById('btnThemNguoiDung').style.display='none';
    document.getElementById('timkiem').style.display='none';
};

// tắt loading
function tatLoading() {
    document.getElementById('loading').style.display='none';
    document.getElementById('btnThemNguoiDung').style.display='block';
    document.getElementById('timkiem').style.display='flex';
};

// reset form
function reset(){
    // form input rỗng
    document.getElementById('Id').style.display='none';
    document.getElementById('IdND').disabled = false;
    document.getElementById('TaiKhoan').value='';
    document.getElementById('HoTen').value='';
    document.getElementById('MatKhau').value='';
    document.getElementById('Email').value='';
    document.getElementById('HinhAnh').value='';
    document.getElementById('loaiNguoiDung').value='0';
    document.getElementById('loaiNgonNgu').value='0';
    document.getElementById('MoTa').value='';
    // span của input rỗng
    document.getElementById('spanTaiKhoan').value='';
    document.getElementById('spanHoTen').value='';
    document.getElementById('spanMatKhau').value='';
    document.getElementById('spanEmail').value='';
    document.getElementById('spanHinhAnh').value='';
    document.getElementById('spanloaiNguoiDung').value='0';
    document.getElementById('spanloaiNgonNgu').value='0';
    document.getElementById('spanMoTa').value='';
    // reset button
    document.getElementById('btnThem').disabled = false;
    document.getElementById('btnCapNhat').disabled = true;
};

// cập nhật loại ND
function updateloaiND(arr){
    arr.forEach(user=>{
        var nd=user;
        if(nd.loaiND==true){
            nd.loaiND='GV';
        };
        if(nd.loaiND==false){
            nd.loaiND='HV';
        };
        axios({
            url:`${BASE_URL}/quanlynguoidung/${user.id}`,
            method:'PUT',
            data:nd,
        });
    });
};

// render danh sách ra màn hình
function renderDanhSach(arr){
    var contentHTML='';
    arr.forEach(user => {
        // // thêm user vô mảng
        // pushND(user);
        // tạo tr
        var contentTr=`<tr>
                    <td>${user.id}</td>
                    <td>${user.taiKhoan}</td>
                    <td>${user.matKhau}</td>
                    <td>${user.hoTen}</td>
                    <td>${user.email}</td>
                    <td>${user.ngonNgu}</td>
                    <td>${user.loaiND}</td>
                    <td>
                        <button class='btn btn-danger' onclick='xoa(${user.id})'>Xóa</button>
                        <button class='btn btn-warning' onclick='sua(${user.id})' data-toggle="modal"
                        data-target="#myModal">Sửa</button>
                    </td>                    
                </tr>`;
        // cộng dồn tr
        contentHTML=contentHTML+contentTr;
    });
    document.getElementById('tblDanhSachNguoiDung').innerHTML=`${contentHTML}`;
};

// đẩy value của user lên form để sửa
function suaThongTin(user){
    // hiển thị id
    document.getElementById('Id').style.display='block';
    // đẩy value lên form
    document.getElementById('IdND').value=user.id;
    document.getElementById('TaiKhoan').value=user.taiKhoan;
    document.getElementById('HoTen').value=user.hoTen;
    document.getElementById('MatKhau').value=user.matKhau;
    document.getElementById('Email').value=user.email;
    document.getElementById('HinhAnh').value=user.hinhAnh;
    document.getElementById('loaiNguoiDung').value=user.loaiND;
    document.getElementById('loaiNgonNgu').value=user.ngonNgu;
    document.getElementById('MoTa').value=user.moTa;
    // disable id
    document.getElementById('IdND').disabled = true;
    // able button cập nhật
    document.getElementById('btnCapNhat').disabled = false;
    
};

// lấy thông tin từ form
function layThongTinTuForm(){
    var id=document.getElementById('IdND').value;
    var taiKhoan=document.getElementById('TaiKhoan').value;
    var hoTen=document.getElementById('HoTen').value;
    var matKhau=document.getElementById('MatKhau').value;
    var email=document.getElementById('Email').value;
    var hinhAnh=document.getElementById('HinhAnh').value;
    var loaiND=document.getElementById('loaiNguoiDung').value;
    var ngonNgu=document.getElementById('loaiNgonNgu').value;
    var moTa=document.getElementById('MoTa').value;
    return{
        id:id,
        taiKhoan:taiKhoan,
        hoTen:hoTen,
        matKhau:matKhau,
        email:email,
        hinhAnh:hinhAnh,
        loaiND:loaiND,
        ngonNgu:ngonNgu,
        moTa:moTa,
    };
};
