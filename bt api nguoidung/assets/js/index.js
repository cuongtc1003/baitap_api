let BASE_URL='https://63c6dcc8dcdc478e15cb1bb4.mockapi.io';
var DSND=[];
var account='';

function fetchND(){
    batLoading();
    axios({
        url:`${BASE_URL}/quanlynguoidung`,
        method:'GET'
    }).then(function(res){
        // update loaiND nếu = true false
        updateloaiND(res.data);
        DSND=[res.data];
        console.log("🚀 ~ file: index.js:13 ~ fetchND ~ DSND", DSND)
        // render danh sách ra màn hình
        renderDanhSach(res.data);
        tatLoading();
    }).catch(function(err){
        console.log(err);
        tatLoading();
    })
};

// chạy fetch lần đầu khi load trang
fetchND();
// thêm user
function them(){
    batLoading();
    var user=layThongTinTuForm();
    var isValid=validation(user);
    if(isValid){
        axios({
            url:`${BASE_URL}/quanlynguoidung`,
            method:'POST',
            data:layThongTinTuForm(),
        }).then(function(res){
            fetchND();
        }).catch(function(err){
            console.log(err);
            tatLoading();
        });
    }else{
        tatLoading();
    } 
};

// xóa user
function xoa(id){
    batLoading()
    axios({
        url:`${BASE_URL}/quanlynguoidung/${id}`,
        method:'DELETE',
    }).then(function(res){
        // render lại khi xóa thành công
        fetchND();
    }).catch(function(err){
        console.log(err);
        tatLoading();
    });
};

// sửa user
function sua(id){
    // disable button Thêm
    document.getElementById('btnThem').disabled = true;
    axios({
        url:`${BASE_URL}/quanlynguoidung/${id}`,
        method:'GET',
    }).then(function(res){
        suaThongTin(res.data);
        account=res.data.taiKhoan;
    }).catch(function(err){
        console.log(err);
        tatLoading();
    });
};

function capNhat(){
    batLoading();    
    var ND=layThongTinTuForm();
    var isValid=validationCapNhat(ND,account);
    if(isValid){
        axios({
            url:`${BASE_URL}/quanlynguoidung/${ND.id}`,
            method: 'PUT',
            data:ND,
        }).then(function(res){
            reset();
            fetchND();
        }).catch(function(err){
            console.log(err);
            tatLoading();
        });
    }else{
        tatLoading();
    }
};

